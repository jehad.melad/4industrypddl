// Agent sample_agent in project jplanner



/* Initial beliefs and rules */

/* Initial goals */
domaine("domaine.pddl").
problem(prob1,"prob1.pddl").
problem(prob2,"prob2.pddl").
problem(prob3,"prob3.pddl").
problem(prob4,"prob4.pddl").
problem(prob5,"prob5.pddl").
problem(prob6,"prob6.pddl").
problem(prob7,"prob7.pddl").
problem(prob8,"prob8.pddl").


/* Rules to handel the LeafGoal */


/* Set of rules to retrive the Leaf Goals*/
// goal is leaf: end condition
hasLeafGoals(goal(Name,_,_,_,_,_,noplan), [Name]) .

// goal is non-leaf: inspect sub-goals as a list 
hasLeafGoals(goal(_,_,_,_,_,_,plan(_,GoalList)), GoalNames) :-
    hasLeafGoals(GoalList, GoalNames) .

// goal list is empty: end condition
hasLeafGoals([], []) .

// goal list has head: inspect head and tail, then merge their leaf goals
hasLeafGoals([Goal|OtherGoals], AllGoalNames) :-
    hasLeafGoals(Goal, GoalNames) &
    hasLeafGoals(OtherGoals, OtherGoalNames) &
    .concat(GoalNames, OtherGoalNames, AllGoalNames) .

/* Goals */
!buildPlan(Domaine, Problem).


/* Plans */

																		
+specification(scheme_specification(Assemb,G,_,_))  <-	
																		?(hasLeafGoals(G,Leafs));
																		
																		for(.member(Problem, Leafs)){
																			initProblem(Problem, PddlProblem);
																			.print("#####################  ",Problem,"  #######################");
																			.print(PddlProblem);
																		};
																		.	

															
			
+!buildPlan(Domaine, Pddl) : domaine(Domaine)   <-
														.print("wait to build a plan ...");
														buildPlan(Domaine,"prob1.pddl", Plan);														 
														.add_plan(Plan);
														// .print(Plan);
														for(getArtifactList(Id)){
															lookupArtifact(Id);
															focus(Id);
															};
															// !attach_leg2; // prob6.pddl
															!deliver_leg1;
														makeArtifact("Name", "tools.Robo2Art",[], ArtId);
														focus(ArtId);
														.



{ include("./agentSpec.asl") }					
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
 { include("$moiseJar/asl/org-obedient.asl") }
