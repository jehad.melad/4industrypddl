(define (problem attach_leg2) 
        (:domain stool)
        (:objects 
                leg2 

                robo2art

                location2
        )
        (:init

            (legs leg2) 
            
            (arm robo2art)
            
            (place location2)
            
            (at leg2 location2)
      
            (at robo2art location2)
            (free robo2art) 
        )
        (:goal (and
                    (attached  leg2 )
 
                )
        )
)
