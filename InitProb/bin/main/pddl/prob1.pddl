(define (problem deliver_leg1) 
        (:domain stool)
        (:objects 
                leg1 
                
                robo1art 

                location1 
                location2
        )
        (:init
            (legs leg1) 

            
            (arm robo1art) 
            
            (place location1) 
            (place location2)
            
            (at leg1 location1) 
            
            (at robo1art location1)
            (free robo1art) 
        )
        (:goal (and
                    (in leg1 location2)
                )
        )
)
