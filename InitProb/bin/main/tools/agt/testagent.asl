// Agent sample_agent in project jplanner



/* Initial beliefs and rules */

/* Initial goals */
domaine("domaine.pddl").
problem(prob1,"prob1.pddl").
problem(prob2,"prob2.pddl").
problem(prob3,"prob3.pddl").
problem(prob4,"prob4.pddl").
problem(prob5,"prob5.pddl").
problem(prob6,"prob6.pddl").
problem(prob7,"prob7.pddl").
problem(prob8,"prob8.pddl").

// set param for operation arti
// build aninitproblem

// plan(A, B). // ,goal(Name,_,plan(_,[goal("G2",_,"noplan")]))
goal("Rooot",_,plan(_, goal("testGoal",_,"noplan"))).
// goal("testGoal",_,"noplan").


/* Rules to handel the LeafGoal */



leafGoal(goal(N,_,Px),_, Name):- goal(N,_,Px) & Px == "noplan" & N = Name  .

leafGoal(goal(N,_,Px),M,Nn):- goal(N,_,Px) & Px \== "noplan" & goal(_,_,plan(_, Z)) & Z = goal(Na,_,Pl) & Nn = Na & M = Pl & leafGoal(goal(Na,_,Pl),_,_).


// leafGoal(GoalName, G):- goal(Name,_,P) & P \== "noplan" &  P =  plan(_, [G|T])  &  leafGoal(_,G).

// leafGoal(GoalName,P):- goal(GoalName,_,_,_,_,_,P) & not P == "noplan" &goal(N,_,_,_,_,_,plan(_,[goal(N,_,_,_,_,_,P)])) & leafGoal(GoalName,P) .

// leafs([]).



/* Goals*/
!buildPlan(Domaine, Problem).
!init.
!cheching.

/* Plans */

// +scheme_specification(Id,GoalsTreeStartingByRootGoal,missions, properties)
																		
// +specification(scheme_specification(Assemb,G,_,_))
+!cheching : true  <-
																	
																		// if(Nmae \== ""  & P \== "noplan"){
																			// .print("this is the leaf : ", GoalName);
																			// .print("root plane", K);
																			.my_name(MyName);
																			.print(MyName);
																			// .send(MyName, tell, P);
																			// .send(MyName, tell, G);
																			?(leafGoal(goal("Rooot",_,plan(_, goal("testGoal",_,"noplan"))),_,GoalName));
																			.print(M,GoalName);

																			
																			// !next;
																		// };
																		// .print(" Test");
																		// initProblem;
																		// WorkSpace
																		.	

// +!next: 
// 			// specification(scheme_specification(Assemb,goal(Nmae,_,_,_,_,_,plan(P1,[goal(_,_,_,_,_,_,_),_])),_,_))
// 			plan(A,G) 
// 						<- 
// 						// .member(X,B);
// 						// .print(X);
// 						// .concat(B,M);
// 						// .print(M);
// 						for( .member(M,G) ){
// 							.print(" M : ", M);
// 							// .print(" X : ", G);
// 							-+M;
							
// 							?(goal(Name,_,_,_,_,_,P));
// 							if(P == "noplan"){
// 								// .queue.create(Leafs);
// 								// .queue.add(Leafs, Name )
// 								};
// 							//adding the plan to a list
// 							// .queue.create(Plans);
// 							// .queue.add(Plans, P );
							
// 						};
// 						// .queue.create(S);
// 						// .queue.add(S, a );
// 						.print(S);

// 						// !next;
// 						// .print(Leafs);

// 						.
															
			
+!buildPlan(Domaine, Pddl) : domaine(Domaine)   <-
														.print("wait to build a plan ...");
														buildPlan(Domaine,"prob6.pddl", Plan);														 
														.add_plan(Plan);
														// .print(Plan);
														for(getArtifactList(Id)){
															lookupArtifact(Id);
															focus(Id);
															};
															!attach_leg2; // prob1.pddl
														makeArtifact("Name", "tools.Robo2Art",[], ArtId);
														focus(ArtId);
														.



{ include("./agentSpec.asl") }					
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
 { include("$moiseJar/asl/org-obedient.asl") }
