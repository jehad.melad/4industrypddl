(define (problem attach_leg1) 
        (:domain stool)
        (:objects 
                leg1 

                robo2art

                location2
        )
        (:init

            (legs leg1) 
            
            (arm robo2art)
            
            (place location2)
            
            (at leg1 location2)
      
            (at robo2art location2)
            (free robo2art) 
        )
        (:goal (and
                    (attached  leg1 )
 
                )
        )
)
