(define (problem deliver_leg3) 
        (:domain stool)
        (:objects 

                leg3 
                
                robo1art 
                robo2art
                
                location1 
                location2
        )
        (:init

            (legs leg3) 
            
            (arm robo1art) 
            (arm robo1art)
            
            (place location1) 
            (place location2)
            

            (at leg3 location1)
            
            (at robo1art location1)
            (free robo1art) 
        )
        (:goal (and

                    (in leg3 location2)

                )
        )
)
