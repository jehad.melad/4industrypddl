## update status:
- The agent able to generate a **problem.pddl** content as a string.
- Ability to retrive the leaf goals from the **functional specification** of the *Organization*.
- The ability to call artifacts that serve speific goals and focus on them.
- Also it is able to get the plan of that problem definition.
## Retrive Artifacts' Operations:

To get the operations of the artifacts and its  params by adding method within the planner class as an new operation.

```
@OPERATION
public void initProblem() throws CartagoException {
        ....
        ....
        
         ICartagoController controller = CartagoService.getController(wps.toString());
         ArtifactId[]	artifactObject = controller.getCurrentArtifacts();
         List<ArtifactId> allArtifacts = Arrays.asList(artifactObject);
         
         ArtifactInfo artifactinfo = controller.getArtifactInfo(artifactName);
         
         for(OpDescriptor op : artifactinfo.getOperations()){
         
             String operationName = op.getOp().getName();
            int paramsNum = op.getOp().getNumParameters();
         }
         
         ....
         ....
    
```

## To Test:

On the project directory on terminal write:

```
\..\...\..\..\InintProb> ./gradlew run
```
The result will be as:

```
artifact list : [robo2art, robo1art]
op list : [[observeProperty, attach], [pick, observeProperty, move, drop]]
param list : [[2, 1], [3, 2, 4, 4]]
```
