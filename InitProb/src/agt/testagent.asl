
/* beliefs */
domaine("domain.pddl").

/* Plans */

+specification(scheme_specification(Assembling_sch,Goal,Missions,_)): domaine(Domain) 
	<-
	?(hasLeafGoals(Goal,Leaves));
	for(.member(Problem, Leaves)){
		?(problem_precondition(Problem,objects(Obj),init(Init), goalState(GoalState)));
		buildPlan(Domain,Problem ,Obj,Init, GoalState, Plan);
		.add_plan(Plan);
		// .print(Plan);
	};
	for(getArtifactList(Id)){
			lookupArtifact(Id);
			focus(Id);
			};
	?(existMission(Missions, MissionNamesList));
	for(.member(MissionName, MissionNamesList)){
		commitMission(MissionName);
	}
	.


{ include("pre-conditions.asl")}
{ include("rules.asl")}
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
 { include("$moiseJar/asl/org-obedient.asl") }
