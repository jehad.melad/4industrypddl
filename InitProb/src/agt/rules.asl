/*
Set of rules to retrive the leafs of the Organization schema
*/

// goal is leaf: end condition
hasLeafGoals(goal(Name,_,_,_,_,_,noplan), [Name]) .

// goal is non-leaf: inspect sub-goals as a list 
hasLeafGoals(goal(_,_,_,_,_,_,plan(_,GoalList)), GoalNames) :-
    hasLeafGoals(GoalList, GoalNames) .

// goal list is empty: end condition
hasLeafGoals([], []) .

// goal list has head: inspect head and tail, then merge their leaf goals
hasLeafGoals([Goal|OtherGoals], AllGoalNames) :-
    hasLeafGoals(Goal, GoalNames) &
    hasLeafGoals(OtherGoals, OtherGoalNames) &
    .concat(GoalNames, OtherGoalNames, AllGoalNames) .



// [mission(goalMission1,1,1,[Goals_Of_This_Mission],[]), Other_Missions]
existMission(mission(Name,_,_,_,_), [Name]).
existMission([],[]).
existMission([Mission|OtherMissions], AllMission):- 
    existMission(Mission,MissionNames)              & 
    existMission(OtherMissions,OtherMissionNames)   & 
    .concat(MissionNames, OtherMissionNames, AllMission).



