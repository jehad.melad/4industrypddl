package tools;

import cartago.Artifact;
import cartago.OPERATION;

import java.util.Arrays;

public class Robo1Art extends Artifact {
    /*
    * Set of action as an OPERATION:
    * pick up
    * move
    * drop
    * */

    @OPERATION
    public void pick(String arg1, String arg2){

        System.out.println("Pick : ( " + arg1+", "+  arg2 + " )");

    }

    @OPERATION
    public void move(String arg1 ){

        System.out.println("Move :( " + arg1+ " )");

    }

    @OPERATION
    public void drop(String arg1, String arg2){

        System.out.println("Drop : ( " + arg1+", "+  arg2 + " )");

    }
}
