## Approaches:
- meta-programming.
- change an agent's plan library.
- MOISE + mission generate.

## R.M.L.O.T.F with MAS for theWeb of Things

### Approach:
In this paper they use Agent & Artifact container which use a custom extention of the MOISE framework for reading organizational specifications in RDF (rather than the usual XML-based specifications).

Software agents and the artifacts they use run in an Agents & Artifacts Container which provide the agent as following:

```
--> A&A container:
    |--> Agent.
        |--> planner Artifact.
        |   |--> synthesize plans in a onblocking manner
        |--> Web interface artifact.
        |   |--> exposes a REST HTTP API
```

Agents use the planner artifact to synthesize plans in a nonblocking manner:

If a solution is found, the notification payload includes a representation of the plan as an RDF Sequence [7] of CArtAgO operation descriptions (such as the one shown in Listing 2). The RDF sequence is translated to an AgentSpeak plan and added to the calling agent’s library of plans.
