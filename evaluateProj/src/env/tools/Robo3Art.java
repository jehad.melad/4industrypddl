// CArtAgO artifact code for project jplanner

package tools;

import cartago.Artifact;
import cartago.OPERATION;

import java.util.Arrays;

public class Robo3Art extends Artifact {
	
	 /*
     * Set of action as an OPERATION:
     * Attach
     * */

    @OPERATION
    public void mount(String arg1){

        // Object[] objectArray = arg;
        // String[] stringArray = Arrays.asList(objectArray).toArray(new String[0]);
        // String args = Arrays.toString(stringArray);
        System.out.println("Mount : ( " + arg1+" )");

    }
}

