+!recover(Goal) 
		<-
		.print(" Try to find recovery plan for : ", Goal);
		?(problem_precondition(Goal,objects(Obj),init(Init), goalState(GoalState)));
		buildPlan("domain.pddl",Goal ,Obj,Init, GoalState, Plan);
		.add_plan(Plan);
		.list_plans;
		.print(Plan);
		!Goal;
		goalAchieved(Goal);
		.print(" Goal achieved  : ", Goal);
		.
		
-!recover(Goal) 
		<-
		.print("Unable to recover the Goal : ", Goal );
		.



{ include("readyplan.asl")}
// { include("pre-conditions.asl")}
{ include("rules.asl")}
