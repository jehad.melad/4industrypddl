// // initprob(problem_Name, [Objects],[init],[goal])
problem_precondition(deliver_leg1, objects(["leg1","Robo1Art", "location1","location2"]),init(["at leg1 location1", "free"]), goalState(["at leg1 location2"])).
problem_precondition(deliver_leg2, objects(["leg2","Robo1Art", "location1","location2"]),init(["at leg2 location1", "free"]), goalState(["at leg2 location2"])).
problem_precondition(deliver_leg3, objects(["leg3","Robo1Art", "location1","location2"]),init(["at leg3 location1", "free"]), goalState(["at leg3 location2"])).
problem_precondition(deliver_leg4, objects(["leg4","Robo1Art", "location1","location2"]),init(["at leg4 location1", "free"]), goalState(["at leg4 location2"])).

problem_precondition(deliver_pad1, objects(["pad1","Robo1Art", "location1","location2"]),init(["at pad1 location1", "free"]), goalState(["at pad1 location2"])).
problem_precondition(deliver_pad2, objects(["pad2","Robo1Art", "location1","location2"]),init(["at pad2 location1", "free"]), goalState(["at pad2 location2"])).
problem_precondition(deliver_pad3, objects(["pad3","Robo1Art", "location1","location2"]),init(["at pad3 location1", "free"]), goalState(["at pad3 location2"])).
problem_precondition(deliver_pad4, objects(["pad4","Robo1Art", "location1","location2"]),init(["at pad4 location1", "free"]), goalState(["at pad4 location2"])).

problem_precondition(attach_leg1, objects(["leg1","Robo2Art", "location2"]),init(["at-position leg1","free"]),goalState(["attach leg1"])).
problem_precondition(attach_leg2, objects(["leg2","Robo2Art", "location2"]),init(["at-position leg2","free"]),goalState(["attach leg2"])).
problem_precondition(attach_leg3, objects(["leg3","Robo2Art", "location2"]),init(["at-position leg3","free"]),goalState(["attach leg3"])).
problem_precondition(attach_leg4, objects(["leg4","Robo2Art", "location2"]),init(["at-position leg4","free"]),goalState(["attach leg4"])).

problem_precondition(mount_pad1, objects(["pad1","Robo2Art", "location2"]),init(["at-position pad1","free"]),goalState(["mount pad1"])).
problem_precondition(mount_pad2, objects(["pad2","Robo2Art", "location2"]),init(["at-position pad2","free"]),goalState(["mount pad2"])).
problem_precondition(mount_pad3, objects(["pad3","Robo2Art", "location2"]),init(["at-position pad3","free"]),goalState(["mount pad3"])).
problem_precondition(mount_pad4, objects(["pad4","Robo2Art", "location2"]),init(["at-position pad4","free"]),goalState(["mount pad4"])).