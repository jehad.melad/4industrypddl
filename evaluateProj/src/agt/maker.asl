+permission(Me,Norm,committed(Me,Mission,Scheme),Deadline)
	:
	.my_name(Me)
	<- 
	+commitment(Me,Mission,Scheme);
	commitMission(Mission)
	.

-!GoalFailed[code(GoalFailed[scheme(ResSchm)])]
	: obligation(_,enabled(ResSchm,GoalFailed),done(ResSchm,GoalFailed,_),_) 
	<-  
	.print("Failed goal is :  ", GoalFailed);
	!recover(GoalFailed)
	.



{ include("common/readyplan.asl")}
{ include("common/pre-conditions.asl")}
{ include("common/rules.asl")}
{ include("common/recovery.asl")}

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
 { include("$moiseJar/asl/org-obedient.asl") }
