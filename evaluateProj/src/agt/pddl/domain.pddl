(define (domain stool)
    (:requirements :negative-preconditions :strips  )
    (:predicates 
        (destination ?loc)
        (at ?leg ?loc)
        (free)  
        (grip ?legs) 
        (at-position ?leg) 
        (attach ?leg)
        (mount ?obj)
    )
    (:action pick
        :parameters ( ?leg ?loc1)
        :precondition (and 
                        (free)
                        (at ?leg ?loc1)
        )
        :effect (and 
                    (not (free))
                    (not (at ?leg ?loc1))
                    (grip ?leg)
        )
    )
    [. . . .]
)

    (:action move
        :parameters (?to)
        :precondition (and 
                            (not (free))
                            (not (destination ?to))
        )
        :effect (and 
                        (not (free))
                        (destination ?to)
                )
        )
    
    (:action drop
        :parameters (?leg ?to)
        :precondition (and 
                            (not (free))
                            (destination ?to)
        )
        :effect (and 
                            (free)
                            (at ?leg ?to)
        )
    )
    (:action attach
        :parameters (  ?leg )
        :precondition (and
                            (at-position ?leg)
                            (free)
        )
        :effect (and
                            (attach ?leg)
        )
    )
    (:action mount
        :parameters (  ?leg )
        :precondition (and
                            (at-position ?leg)
                            (free)
        )
        :effect (and
                            (mount ?leg)
        )
    )


    
)
