## jacamo

```
// // +obligation(Ag,enabled(resSchm,deliver_leg4),done(resSchm,deliver_leg4,Ag),_)<- .print("#### test").
+!recovery: domaine(Domain) & goalState(resSchm,Problem,[Ag],[],enabled) & obligation(Ag,enabled(resSchm,Problem),done(resSchm,Problem,Ag),_) 
							<-
							?(goalState(resSchm,Problem,[Ag],[],satisfied));
							.print("Looking for a recovery plan for the problem : ",Problem);
							
							?(xproblem_precondition(Problem,objects(Obj),init(Init), goalState(GoalState)));
							buildPlan(Domain,Problem ,Obj,Init, GoalState, Plan);
							.add_plan(Plan);
							// .list_plans;

							.print("Plan found...", Plan);
							.print("Excute the recovery plan ....")
							!Problem;
                            goalAchieved(Problem);

							.wait(500);
							!recovery		
							.

-!recovery: domaine(Domain) & goalState(resSchm,Problem,[Ag],[],enabled) & obligation(Ag,enabled(resSchm,Problem),done(resSchm,Problem,Ag),_) 
							<-
							.wait(500);
							.print("The goals all accomplished !!");
							.
```