# Notes:
## **Plan failure:**
### Causes:
* Lack of relevant or applicable plans for an achievement goal.
    
    **Explaination:**
    This happen when the *plan* excuted while it require a *subgoal* to be achieved earlier. Also the plan could be known but the context does not match the agen's beliefs therefore the plan will not be applicable.
* 


## **jason code:**
### ObsProp
*https://ai4industry.sciencesconf.org/data/Multi_Agent_Systems_lecture.pdf*.  (p. 107 to 111)
### signals:
* oblCreated(o): the obligation o is created
* oblFulfilled(o): the obligation o is fulfilled  : This will retrive the goalstate obligation that has satisfied state.
* oblUnfulfilled(o): the obligation o is unfulfilled (e.g. by timeout)
* oblInactive(o): the obligation o is inactive (e.g. its maintenance condition does not hold anymore)
* normFailure(f): the failure f has happened (e.g. due some regimentation)

### Jason book
* Plan Failure ( *4.2 Plan Failure (p.86)* ).

### github examples

* house-building : *https://github.com/jacamo-lang/jacamo/blob/master/examples/house-building/src/agt/giacomo.asl*.
* auction : *https://github.com/jacamo-lang/jacamo/blob/master/examples/auction/src/agt/auction_capabilities.asl*.
* Goal not achieved : *https://github.com/jacamo-lang/jacamo/blob/master/examples/auction/src/agt/auction_capabilities.asl*
    ```
    ..
    ...
    +oblUnfulfilled( obligation(Ag,_,done(Sch,bid,Ag),_ ) )[artifact_id(AId)]  // it is the case that a bid was not achieved
   : .my_name(Me) & play(Me,auctioneer,_)  // handle unfulfilled obl if I am the auctioneer
   <- .print("Participant ",Ag," didn't bid on time! S/he will be placed in a block list");
       // TODO: implement a block list artifact
       admCommand("goalSatisfied(bid)")[artifact_id(AId)]. // go on in the scheme....
    ```


### **MOISE:**
* The *MOISE* runs directly after running the *JACAMO* as a result we obtain the formation statuse of the organizational sepcifications (i.e. as follow):
```
...
formationStatus(ok)
commitment(testagent,goalMission1,resSchm)[...].
commitment(testagent,goalMission2,resSchm)[...].
...
goalState(resSchm,assembleStool,[],[],waiting)[...].
goalState(resSchm,attach_leg1,[testagent],[],waiting)[...].
goalState(resSchm,attach_leg2,[testagent],[],waiting)[...].
goalState(resSchm,attach_leg3,[testagent],[],waiting)[...].
goalState(resSchm,attach_leg4,[testagent],[],waiting)[...].
goalState(resSchm,deliver_leg1,[testagent],[],enabled)[...].
goalState(resSchm,deliver_leg2,[testagent],[],waiting)[...].
goalState(resSchm,deliver_leg3,[testagent],[],waiting)[...].
goalState(resSchm,deliver_leg4,[testagent],[],waiting)[...].
goalState(resSchm,mount_leg1,[],[],waiting)[...].
goalState(resSchm,mount_leg2,[],[],waiting)[...].
goalState(resSchm,mount_leg3,[],[],waiting)[...].
goalState(resSchm,mount_leg5,[],[],waiting)[...].
...
normative_board(stool_gr.resSchm,cobj_10)[...].
obligation(testagent,enabled(resSchm,deliver_leg1),done(resSchm,deliver_leg1,testagent),1656668467261)[...].
...
parentGroup(root)[...].
play(testagent,role1,stool_gr)[...].
play(testagent,role2,stool_gr)[...].
scheme(resSchm,assembling_sch,cobj_9)[...].
schemes([resSchm])[...].
...

```

### //commented lines:

```
// +!run: obligation(Ag,enabled(Sch,deliver_leg1),done(Sch,deliver_leg1,Ag),_)[norm(ngoal,[_,_,_,["G",deliver_leg1],["M",goalMission1],_])] 
// 											<- commitMission(goalMission1).

// +obligation(Ag,enabled(resSchm,G),done(resSchm,G,Ag),_).
// +!run: obligation(Ag,enabled(resSchm,G),done(resSchm,G,Ag),_)[norm(ngoal,[_,_,_,["G",G],["M",M],_])] 
// 																				<-
// 																				.print("I will commit to the mission : ", M);
// 																				// commitMission(M);
// 																				.


// +!run: obligation(Ag,enabled(Sch,deliver_leg1),done(Sch,deliver_leg1,Ag),_)[norm(ngoal,[_,_,_,["G",deliver_leg1],["M",goalMission1],_])] 
// 											<- commitMission(goalMission1).
```

* jacamo run the next events even if there is an error log.
* try to use obfullfild to work just with things that not fullfilled.
* Set a time for each problem individual will insure that the state of the problem will change.
* +permission() ==> this to handle the mission with the right agent who predefined with a role. also this is true when the type of the norm in XML file set to permission. (i.e. commit to the mission by the agent itself):
    ```
    <norm id="n1" type="permission" role="role1" mission="goalMission1"/>
    ```
*  In case of  *type="obligation"* we do not need to set *commitMission(MissionName)* within the agent code:
    ```
    <norm id="n1" type="obligation" role="role1" mission="goalMission1"/>
    ```