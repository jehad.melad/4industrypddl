## MOISE+ (MAS Organization):

Ref. example. http://moise.sourceforge.net/doc/tutorial.pdf#page=11&zoom=100,132,65.

We can define this organization as a set of behaviour constraints by means, there will be a roles to play and to follow.

Organizational constrains are useful in open MAS: the kind of agent that will enter the system is not known. Therefore, some constraints should be enforced.

In MAS Organization, It has three main dimentions:

```
.
├─ Organizational dimention.
└──├─ Functional Dimention.
   ├─ Structural dimention.
   └─ Normative Dimention.
```
more details:

**Functional Dimention:**

It handles: 
- The specification of the plan. 
- policies to allocate tasks to agents.
- The coordination of the plan excution.
- The quality (e.g. Time comsumption, TTF)
```
.
├─ Functional Dimention. (i.e. set of schemas tell how MAS achieve global goals)
    └── Schemas.
        ├─ Missions 
        └─ Goals (i.e. the global goals decomposed by plan and distributed to agents by missions).
            ├─ subgoals.
            └─ Operation.
                ├─ Sequence.
                ├─ Paralism.
                └─ Choice
```
**Structural dimention:**

It handles: 
- The definition of the roles. 
- The group specification of the roles.
- The relation between the defined roles and groups (i.e. intra-group or inter-group).
    - `Intra-group`, an agent with *source role* in group G is linked to all agents in the same group or sub-group (i.e. hierarchy ).
    - `Inter-group`, an agent with *source role* linked to all other agents and it does not care which groups these agents belong to.

The norm will handle the autonomy of the agents by explicitly what is permitted and obligated in the organization (i.e. Agent should obey the norm specification). 
```
.
├─ Structural Dimention. 
    ├─ Individual level.
    |   └─ Behaviour of the agent to the role.
    |
    ├─ Society level.
    |   └─ Acquaintance. || Communication. || Authority.
    |
    └─ Collective level.
        └─ Aggregation of the roles in group.
```
**Normative dimention:**
```
.
├─ Normative Dimention. 
    ├─ Permision (i.e. Allow).
    └─ Obligation (i.e. Ought or Must).
```

**Note:**
assuming that just one dimention considered:

In case of *`Functional dimention`*, in this situation the organization has nothing to *Tell* an agent when there is no plan to excute.

In case of *`Structural dimention`*, agents have to reason about the collective plan everytime they interact with each other.

` However`, 2 dimention togather will help agents to have more information to reason about the others position  in the organization then will lead to better interact.

Here we link the both aforementioned dimentions by the 3rd dimention ( Normative dimention). MAS will be able to change the own Structural organization without changing the functional organization and vice versa.

---
![plan](./figures/plan.png)
`figure: the plan`

---

This table show the normative specification (NS)
|role|  deontic relation |  mission |  
|---|---|---|
|gripper |  obligation |  m1 | 
|attaching| permision  | m2  |


This table show the organizational entity (*OE*)
|Agent|  role |  in group |  
|---|---|---|
|Ag1 |  gripper |  stool | 
|Ag2| attaching  | stool  |
----
## First result after settingup the Organization file `org.xml`:
it is a simple print goals written in `agentA.asl`
```
+!deliver_leg1 <- println("deliver_leg1").
+!attach_leg1 <- println("attach_leg1").


+!deliver_leg2 <- println("deliver_leg2").
+!attach_leg2 <- println("attach_leg2").

+!deliver_leg3 <- println("deliver_leg3").
+!attach_leg3 <- println("attach_leg3").

+!deliver_leg4 <- println("deliver_leg4").
+!attach_leg4 <- println("attach_leg4").
```
**Result**

```
CArtAgO Http Server running on http://192.168.56.1:3273
Jason Http Server running on http://192.168.56.1:3272
[Moise] Workspace plannerOrg created.
[Moise] getJoinedWorkspaces: [plannerOrg]
Moise Http Server running on http://192.168.56.1:3271
[Moise] scheme created: schm: assembling_sch using artifact SchemeBoard
[Moise] group created: stoolGr: stool_gr using artifact ora4mas.nopl.GroupBoard
[robo] joinned workspace plannerOrg
[human] joinned workspace plannerOrg
[robo] focusing on artifact stoolGr (at workspace plannerOrg) using namespace default
[human] focusing on artifact stoolGr (at workspace plannerOrg) using namespace default
[human] focusing on artifact plannerOrg (at workspace plannerOrg) using namespace default
[robo] focusing on artifact plannerOrg (at workspace plannerOrg) using namespace default
[robo] I am obliged to commit to deliverMission on schm... doing so
[robo] deliver_leg1
[human] attach_leg1
[robo] deliver_leg2
[human] attach_leg2
[robo] deliver_leg3
[human] attach_leg3
[robo] deliver_leg4
[human] attach_leg4
```

![test Org](./figures/orgTest.org)


we able to get usefull information by writing the http://192.168.56.1:3271/ in browser.

  