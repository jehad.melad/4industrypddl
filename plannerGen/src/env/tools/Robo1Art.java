package tools;

import cartago.Artifact;
import cartago.OPERATION;

import java.util.Arrays;

public class Robo1Art extends Artifact {
    /*
    * Set of action as an OPERATION:
    * pick up
    * move
    * drop
    * */

    @OPERATION
    public void pick(String arg1, String arg2, String arg3){

        // convert the arg OBJECT --> arg Array string
        // Object[] objectArray = arg;
        // String[] stringArray = Arrays.asList(objectArray).toArray(new String[0]);
        // String args = Arrays.toString(stringArray);
        System.out.println("Pick : ( " + arg1+", "+  arg2+", "+ arg3 + " )");

    }

    @OPERATION
    public void move(String arg1, String arg2, String arg3, String arg4){

        // Object[] objectArray = arg;
        // String[] stringArray = Arrays.asList(objectArray).toArray(new String[0]);
        // String args = Arrays.toString(stringArray);
        System.out.println("Move :( " + arg1+", "+  arg2+", "+ arg3 +", "+ arg1 + " )");

    }

    @OPERATION
    public void drop(String arg1, String arg2, String arg3, String arg4){

        // Object[] objectArray = arg;
        // String[] stringArray = Arrays.asList(objectArray).toArray(new String[0]);
        // String args = Arrays.toString(stringArray);
        System.out.println("Drop : ( " + arg1+", "+  arg2+", "+ arg3 +", "+ arg1 + " )");

    }
}
