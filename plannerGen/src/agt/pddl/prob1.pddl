(define (problem deliver_leg1) 
        (:domain stool)
        (:objects 
                leg1 
                ; leg2 
                ; leg3 
                ; leg4 
                
                robo1art 
                robo2art 
               
                
                location1 
                location2
        )
        (:init
            (legs leg1) 
        ;     (legs leg2) 
        ;     (legs leg3) 
        ;     (legs leg4)
            
            (arm robo1art) 
            (arm robo2art)
            
            (place location1) 
            (place location2)
            
            (at leg1 location1) 
        ;     (at leg2 location1)
        ;     (at leg3 location1)
        ;     (at leg4 location1)
            
            (at robo1art location1)
            (free robo1art) 
        )
        (:goal (and
                    (in leg1 location2)
                ;     (in leg2 location2)
                ;     (in leg3 location2)
                ;     (in leg4 location2)
                )
        )
)
