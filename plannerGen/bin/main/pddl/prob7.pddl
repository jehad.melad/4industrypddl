(define (problem attach_leg3) 
        (:domain stool)
        (:objects 
                leg3 

                robo2art

                location2
        )
        (:init

            (legs leg3) 
            
            (arm robo2art)
            
            (place location2)
            
            (at leg3 location2)
      
            (at robo2art location2)
            (free robo2art) 
        )
        (:goal (and
                    (attached  leg3 )
 
                )
        )
)
