(define (problem deliver_leg4) 
        (:domain stool)
        (:objects 

                leg4 
                
                robo1art 
                robo2art
                
                location1 
                location2
        )
        (:init

            (legs leg4)
            
            (arm robo1art) 
            (arm robo1art)
            
            (place location1) 
            (place location2)
            

            (at leg4 location1)
            
            (at robo1art location1)
            (free robo1art) 
        )
        (:goal (and

                    (in leg4 location2)
                )
        )
)
