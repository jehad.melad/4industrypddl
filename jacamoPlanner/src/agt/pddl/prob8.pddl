(define (problem attach_leg4) 
        (:domain stool)
        (:objects 
                leg4 

                robo2art

                location2
        )
        (:init

            (legs leg4) 
            
            (arm robo2art)
            
            (place location2)
            
            (at leg4 location2)
      
            (at robo2art location2)
            (free robo2art) 
        )
        (:goal (and
                    (attached  leg4 )
 
                )
        )
)
