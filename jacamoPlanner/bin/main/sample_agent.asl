// // Agent sample_agent in project jplanner

// /* Initial beliefs and rules */

// /* Initial goals */

// !start.

// /* Plans */

// +!start : true <- 
//                     makeArtifact("myplanner", "tools.Planner1",[],Id);
//                     focus(Id);
//                     .print(Id);
//                     .print("hello world.");
// //                    inc("lol").
// 					myplanner.
					
// +planner <- print("planner start").
// +tick(V) <- print(V).
                    
// //+count(V, B) <- println("obs new value ", V , " ",B).

// //+tick(ID) <- println("perceived a tick", ID).



// { include("$jacamoJar/templates/common-cartago.asl") }
// { include("$jacamoJar/templates/common-moise.asl") }

// // uncomment the include below to have an agent compliant with its organisation
// // { include("$moiseJar/asl/org-obedient.asl") }


// #######################################################################################################################################


// +!deliver_leg1 <- println("deliver_leg1").
// +!attach_leg1 <- println("attach_leg1").


// +!deliver_leg2 <- println("deliver_leg2").
// +!attach_leg2 <- println("attach_leg2").

// +!deliver_leg3 <- println("deliver_leg3").
// +!attach_leg3 <- println("attach_leg3").

// +!deliver_leg4 <- println("deliver_leg4").
// +!attach_leg4 <- println("attach_leg4").

// /* Plans */

// +!start : true <- 
//                     makeArtifact("test", "tools.Planner1",[],GrArtId);
//                     focus(GrArtId);
//                     .println("Artifact id: ",GrArtId);
// 					myplanner("src\\agt\\pddl\\domain.pddl","src\\agt\\pddl\\problem.pddl").
					
					
// 				// 	createGroup(test_group, stoolGr, GrArtId);
// 				// 	adoptRole(gripper)[artifact_id(GrArtId)];
//       			// 	focus(GrArtId);
      				
//       			// 	// create the scheme
// 			    //    createScheme(bhsch, schm, SchArtId);
// 			    //    focus(SchArtId);
			       
			       
// 			    //   ?formationStatus(ok)[artifact_id(GrArtId)]; // see plan below to ensure we wait until it is well formed
// 			    //   addScheme("bhsch")[artifact_id(GrArtId)];
					
// 				// .

// // run the orgGoals
// +permission(A,_,committed(A,M,S),_)
//     : .my_name(A) & // the permission is for me
//       my_mission(M) // my mission is M
// <- commitMission(M).
										


// // +planner(Plan)<- .print(Plan).

//############################################################## test output of the plan ############################
// +!mytool: true <- makeArtifact("c0","tools.Actions",[],C);
// 														focus(C).
// +tick(T)<- .print(T).

// !goal.
// +!goal : true <- 
// 				makeArtifact("c0","tools.Actions",[],Id);
// 				focus(Id);
				
// 				pick([ arm1 , leg1 , location1 ]);
// 				move([ arm1 , leg1 , location1 , location2 ]);
// 				drop([ arm1 , leg1 , location1 , location2 ])
// 		
// #############################################  artifact ###############################
// http://jacamo.sourceforge.net/cartago/doc/api/cartago/WorkspaceArtifact.html
// getCurrentArtifacts(N);
// .print(N);
// ["node","console","blackboard","JaCaMoLauncherAgEnv-body","workspace","agentB-body","robo2art","robo1art","manrepo","planner"]
														
