(define (problem deliver_leg2) 
        (:domain stool)
        (:objects 
                leg2 

                robo1art 
                robo2art
                
                location1 
                location2
        )
        (:init

            (legs leg2) 
            
            (arm robo1art) 
            (arm robo1art)
            
            (place location1) 
            (place location2)
            
            (at leg2 location1)
      
            (at robo1art location1)
            (free robo1art) 
        )
        (:goal (and
                    (in leg2 location2)
 
                )
        )
)
