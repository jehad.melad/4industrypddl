// Agent sample_agent in project jplanner

/* Initial beliefs and rules */

/* Initial goals */
domaine("domaine.pddl").
problem(prob1,"prob1.pddl").
problem(prob2,"prob2.pddl").
problem(prob3,"prob3.pddl").
problem(prob4,"prob4.pddl").
problem(prob5,"prob5.pddl").
problem(prob6,"prob6.pddl").
problem(prob7,"prob7.pddl").
problem(prob8,"prob8.pddl").

/* Goals*/
!buildPlan(Domaine, Problem).


/* Plans */

+!buildPlan(Domaine, Pddl) : domaine(Domaine)   <-
														.print("wait to build a plan ...");
														for(problem(A,B)){
															buildPlan(Domaine,B, Plan);														 
															.add_plan(Plan);
															//  .print(Plan);
															for(getArtifactList(Id)){
																lookupArtifact(Name, Id);
																focus(Id);
															};
														};
														// .list_plans(Plan);
														?(my_mission(M));
														commitMission(M);	
													
														.
	

					
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
 { include("$moiseJar/asl/org-obedient.asl") }
