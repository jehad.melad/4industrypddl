## Run the planner **pddl4j** :

In this part you able to test your own PDDL files and get a result based on your problem assumption. We use this method to have a simple running example of our problem.

First of all, you need to clone the repository on your local device. Then try to build the project ( *pddl4j* ) using **gradle** commads in proper directory:
```
./gradlew build
```
After building the project, try to run the planner with the following command:
```
java -javaagent:build/libs/pddl4j-3.8.3.jar -server -Xms2048m -Xmx2048m fr.uga.pddl4j.planners.statespace.StateSpacePlannerFactory -o stagePDDL/domain.pddl -f stagePDDL/problem.pddl
```

**Result:**

For now we can see some results based on our assumption where we still need to improve. (**updated result**)
```
Results:

encoding problem done successfully (40 ops, 28 facts)
* starting A*
* A* succeeded

found plan as follows:

00: (          pick arm1 leg4 location1) [1]
01: (move arm1 leg4 location1 location2) [1]
02: (          drop arm1 leg4 location2) [1]
03: (          pick arm1 leg2 location1) [1]
04: (move arm1 leg2 location1 location2) [1]
05: (          drop arm1 leg2 location2) [1]
06: (          pick arm1 leg1 location1) [1]
07: (move arm1 leg1 location2 location1) [1]
08: (          drop arm1 leg1 location2) [1]
09: (          pick arm1 leg3 location1) [1]
10: (move arm1 leg1 location1 location2) [1]
11: (          drop arm1 leg3 location2) [1]

plan total cost: 12.00


time spent:       0.03 seconds parsing
                  0.03 seconds encoding
                  0.02 seconds searching
                  0.08 seconds total time

memory used:      0.02 MBytes for problem representation
                  0.03 MBytes for searching
                  0.06 MBytes total

```
## PDDL ( *Planning Domain Definition Language* ):

To start with PDDL we need to look at the main component of PDDL which is:
* Objects:
    * Things that interst us in our domain.
* Predicats:
    * Properties of the **objects** in which it can be *True* or *False*.
* Initial State:
    * The state of the world of things that we start with. ( How it looks like? ).
* Goal Specifications:
    * Thing that we want to be true ( Things need to achieve ).
* Action/Operation:
    * Ways of changing the state of the world. ( How do we chaneg the world of the things that we are interested in to achieve our **Goals** )

## Assembling customized furniture domain:

* **Objects:**
   
    *PDDL*
    ```
    (:objects 
                leg1 leg2 leg3 leg4 
                arm1 
                arm2
                location1 
                location2
    )
    ```
* **Predicats:**
   
    *PDDL*
    ```
    (:predicates 
    (legs ?leg)
    (arm ?arm)
    (place ?loc)
    (destination ?loc1 ?loc2)
    (at ?leg ?loc)
    (in ?leg ?loc)
    (moved ?arm) ;
    (free ?arm)  ; ture iff arm is free
    (gripe ?arm ?leg)   ;true iif the arm gripped the leg
    (hold ?arm)  ; true iff arm1 hold somethiing
    )
    ```
* **Initial State:**
   
    *PDDL*
    ```
    (:init
            (legs  leg1) (legs leg2) (legs leg3) (legs leg4)
            (arm arm1) (arm arm2)
            (place location1) (place location2)
            (at leg1 location1) 
            (at leg2 location1)
            (at leg3 location1)
            (at leg4 location1)
            (at arm1 location1)
            ;(not (move arm1))
            (free arm1) 
        )
    ````
    **NOTE**: 
    for simplisity we will assume the robotics move from place1 to place2 without coorditnation.
    
   
* **Goal Specifications:**
   
    *PDDL*
    ```
    (:goal (and
                    (in leg1 location2)
                    (in leg2 location2)
                    (in leg3 location2)
                    (in leg4 location2)
        )
    ```
* **Action/Operation:**
   
    *PDDL*
    ```
    (:action drop
    :parameters (?a ?b ?y ?z)
    :precondition (and 
                    (LOCATION ?b)
                    (GRIPPER ?a)
                    (LEG ?y) 
                    (free ?z)
                    (at-assemble ?z ?a)
    )
    :effect (and
        ; record that we said hello
        (grab ?z ?y) (move ?z ?b)
        (not (free ?z)) 
        (not (at-assemble ?z ?a))
    )
    )
    ```
    
    ```
    (:action grapping
    :parameters (?a ?b ?y ?z)
    :precondition (and 
                    (LOCATION ?a) 
                    (LOCATION ?b)
                    (LEG ?y) 
                    (free ?z)
                    (at-assemble ?z ?a)
    )
    :effect (and
        ; record that we said hello
        (grab ?z ?y) (move ?z ?b)
        (not (free ?z)) 
        (not (at-assemble ?z ?a))
    )
    )
    ```
    
    ```
    (:action grapping
    :parameters (?a ?b ?y ?z)
    :precondition (and 
                    (LOCATION ?a) 
                    (LOCATION ?b)
                    (LEG ?y) 
                    (free ?z)
                    (at-assemble ?z ?a)
    )
    :effect (and
        ; record that we said hello
        (grab ?z ?y) (move ?z ?b)
        (not (free ?z)) 
        (not (at-assemble ?z ?a))
    )

    )
    ```

