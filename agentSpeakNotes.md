# Agent speak ( Jason):
- based on three terms Belief-Desire-Intention *BDI*.
    - Belief - the current state of the world.
        - Goals
    - Desire - ideal future state that agent want.
        - State ( intention-to-achieve) or
        - Plane (intention-to-do).
    - Intention -a subset of desires one is committed to achieve but there are constraints limit the interntioins, e.g. resources.
- Plan - a set of actions to acheive intentions.
- Constructs:
    - Beliefs : Facts about the state of environment (i.e. state of object).
    - Goals : refered by ***!*** sign, Formula to determine what to do.
    - Events : Agent behaviour drivers.
    - plan : Map behaviours to the events that trigger them.
    
# PDDL
