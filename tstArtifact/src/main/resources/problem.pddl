(define (problem assemble)
        (:domain stool)
        (:objects
                leg1
                arm1
                location1
                location2
        )
        (:init
            (legs leg1)
            (arm arm1)
            (place location1)
            (place location2)
            (at leg1 location1)
            (at arm1 location1)
            (free arm1)
        )
        (:goal (and
                    (in leg1 location2)
                )
        )
)
