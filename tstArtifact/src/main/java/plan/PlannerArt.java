package plan;

import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.parser.*;
import fr.uga.pddl4j.planners.Planner;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.planners.statespace.AbstractStateSpacePlanner;
import fr.uga.pddl4j.planners.statespace.StateSpacePlannerFactory;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.Plan;
import org.apache.logging.log4j.core.util.FileUtils;
import org.apache.logging.log4j.core.util.JsonUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class PlannerArt {

    public static void main(String[] args) throws IOException {

        PlannerArt agent = new PlannerArt();
                agent.myplanner();
    }

    public void myplanner() throws IOException {

        // PDDL files to be triggerd domain and problem
        File domain = new File("src\\main\\resources\\domain.pddl");
        String domainx = Files.readString(Path.of("src\\\\main\\\\resources\\\\domain.pddl"));

        System.out.println("Domain"+domainx);

        File problem = new File("src\\main\\resources\\problem.pddl");
        // the methon that will run them

        String problemx = "(define (problem  deliver_leg1) \n" +
                "\t (:domain  stool) \n" +
                "\t (:objects \n" +
                "\t\tleg1\n" +
                "\t\tRobo1Art\n" +
                "\t\tlocation1\n" +
                "\t\tlocation2\n" +
                "\t\t\n" +
                "\t)\n" +
                "\t (:init \n" +
                "\t\t(at leg1 location1)\n" +
                "\t\t(free)\n" +
                "\t\t\n" +
                "\t)\n" +
                "\t (:goal \t (and \n" +
                "\t\t(at leg1 location2)\n" +
                "\t\t) \n" +
                " \t ) \n" +
                " )";
        System.out.println("############################# probelm"+problemx);


        final StateSpacePlannerFactory stateSpacePlannerFactory = StateSpacePlannerFactory.getInstance();
        final Planner.Name plannerName = AbstractStateSpacePlanner.DEFAULT_PLANNER;
        final AbstractStateSpacePlanner planner = stateSpacePlannerFactory.getPlanner(plannerName);
        // Creates the problem factory
        final ProblemFactory factory = ProblemFactory.getInstance();
        if(!problemx.isEmpty()) {
            // Parses the PDDL domain and problem description
            factory.parseFromString(domainx, problemx);
            // Encodes and instantiates the problem in a compact representation
            final CodedProblem pb = factory.encode();


            // #######################################
            Parser parso = new Parser();
            parso.parseProblem(problem);
            parso.parseFromString(domainx);
            String na = parso.getDomain().getName().toString();
            System.out.println("Name #########" + na);
//        factory.encode();
//        ErrorManager r = parso.getErrorManager();
//
////        final CodedProblem o = parso
            // #######################################


            final Plan plan = planner.search(pb);
            StringBuilder sb = new StringBuilder();
            String planToString = "+!goal : true <- ";
            sb.append(planToString);


            List<String> constants = pb.getConstants();
            System.out.println(constants.get(0));
            for (int i = 0; i < plan.size(); i++) {
                // List<String> arr = new ArrayList<>();
                String action = plan.actions().get(i).getName();
                sb.append("task( " + action + ", [");
                int numOfParam = plan.actions().get(i).getArity();
                for (int j = 0; j < numOfParam; j++) {

                    int key = plan.actions().get(i).getValueOfParameter(j);
                    String param = constants.get(key);
                    // arr.add(param);
                    sb.append(" " + param + " ");
                    if (j == numOfParam - 1) {
                        sb.append("");
                    } else {
                        sb.append(",");
                    }
                }
                if (i == plan.size() - 1) {
                    sb.append("])\n");
                } else {
                    sb.append("]);\n");
                }
            }
            sb.append(".");

            // String planToString = "+!goal : true <- pick(); move(); drop().";
            String res = sb.toString();
        }else{
            System.out.println("Error! try to check.");
        }


    }
}
