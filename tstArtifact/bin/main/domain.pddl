(define (domain stool)
    (:requirements :negative-preconditions :strips)

    (:predicates
        (legs ?leg)
        (arm ?arm)
        (place ?loc)
        (destination ?loc2)
        (at ?leg ?loc)
        (in ?leg ?loc)
        (moved ?arm) ;
        (free ?arm)  ; ture iff arm is free
        (gripe ?arm ?leg ?loc)   ;true iif the arm gripped the leg
        ; (hold ?arm)  ; true iff arm1 hold somethiing
    )
    (:action pick
        :parameters (?arm ?leg ?loc)
        :precondition (and
                            (legs ?leg)
                            (arm ?arm)
                            (place ?loc)
                            (free ?arm)
                            (at ?leg ?loc)
        )
        :effect (and
                            (gripe ?arm ?leg ?loc)
                            ; (hold ?arm)
                            (not (free ?arm))
                            (not (at ?leg ?loc))
        )
    )
    (:action move
        :parameters (?arm ?leg ?from ?to)
        :precondition (and

                            (legs ?leg)
                            (place ?from)
                            (place ?to)
                            (arm ?arm)
                            (gripe ?arm ?leg ?from)
                            ; (hold ?arm)
                            (not (free ?arm))
                            (not (moved ?arm))
        )
        :effect (and
                        (moved ?arm)
                        (destination ?to)
                        (gripe ?arm ?leg ?from)
        )
    )
    (:action drop
        :parameters (?arm ?leg ?from ?in)
        :precondition (and
                            (legs ?leg)
                            (arm ?arm)
                            (place ?in)
                            (gripe ?arm ?leg ?from)
                            ; (hold ?arm)
                            (moved ?arm)
                            (destination ?in)
                            (not (free ?arm))
        )
        :effect (and
                            (free ?arm)
                            ; (not (hold ?arm ))
                            (in ?leg ?in)
                            (not (gripe ?arm ?leg ?from) )
                            (not (moved ?arm))
        )
    )
    ; (:action move-back
    ;     :parameters (?arm )
    ;     :precondition (and
    ;                         (free ?arm)
    ;                         (not (moved ?arm))
    ;     )
    ;     :effect (and
    ;                         (free ?arm)
    ;     )
    ; )
)
