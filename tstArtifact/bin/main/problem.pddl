(define (problem assemble)
        (:domain stool)
        (:objects
                leg1
                leg2
                leg3
                leg4

                arm1
                arm2

                location1
                location2
        )
        (:init
            (legs leg1)
            (legs leg2)
            (legs leg3)
            (legs leg4)

            (arm arm1)
            (arm arm2)

            (place location1)
            (place location2)

            (at leg1 location1)
            (at leg2 location1)
            (at leg3 location1)
            (at leg4 location1)

            (at arm1 location1)
            (free arm1)
        )
        (:goal (and
                    (in leg1 location2)
                    (in leg2 location2)
                    (in leg3 location2)
                    (in leg4 location2)
                )
        )
)
