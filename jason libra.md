## change an agent's plan library ( *jason* ):

Link: http://jason.sourceforge.net/Jason.pdf.

- Jason is the **first fully-fledged interpreter** for a much improved version of AgentSpeak, including also speech-act based inter-agent communication.
- Using SACI, a Jason multi-agent system can be distributed over a network effortlessly.
- one important characteristic of AgentSpeak is its theoretical foundation; work on formal verification of AgentSpeak systems is also underway.
- Jason in comparison with other BDI agent systems is that it is implemented in Java (thus multi-platform) and is available Open Source, and is distributed under GNU LGPL
- Besides interpreting the original AgentSpeak(L) language, Jason also features:
    - strong negation, so both closed-world assumption and open-world are available;
    - handling of plan failures;
    - speech-act based inter-agent communication (and belief annotations on information sources);
    - annotations on plan labels, which can be used by elaborate (e.g., decision theoretic) selection functions;
    - support for developing Environments (which are not normally to be programmed in AgentSpeak; in this case they are programmed in Java);
    - the possibility to run a multi-agent system distributed over a network (using SACI);
    - fully customisable (in Java) selection functions, trust functions, and overall agent  rchitecture (perception, belief-revision, inter-agent communication, and acting);
    - a library of essential “internal actions”;
    - straightforward extensibility by user-defined internal actions, which are programmed in Java.
    - a multi-agent system can be easily configured to run on various hosts.

## AgentSpeak ( L):
distinguishes two types of goals:
- achievement goals.
    - The agent want to achieve the state of thr world.
- test goals.
    - returns a unification for the associated predicate with one of the agent’s beliefs; they fail otherwise. ( test if the variable value of the predicate true or not)
- intentions are course of actions to which an agent has committed in order to handle certain events.
    - Each intention is a stack of partially instantiated plans. where the events can be external or internal.
        - external event is originating from perception of the agent’s environment t (i.e. addition and deletion of beliefs based on perception are external events).
        - internal event is generated from the agent’s own execution of a plan (i.e., a subgoal in a plan generates an event of type “addition of achievement goal”).
    - The event selection function `(SE )` selects a single event from the set of events; another selection function `(SO)` selects an “option” (i.e., an applicable plan) from a set of applicable plans; and a third selection function `(SI)` selects one particular intention from the set of intentions.


## plan(s) to the agent\'s plan library"

the plan definition should be added in someplace 