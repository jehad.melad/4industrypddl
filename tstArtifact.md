# Test Artifact

I use this project to test the PDDL4J planner. 
```
./gradlew build
```
```
./gradlew run
```

Using this template we have the minimum usefull information from the PDDL4J. In which we got a result as following:

```
found plan as follows:

00: (          pick arm1 leg2 location1) [1]
01: (move arm1 leg2 location1 location2) [1]
02: (drop arm1 leg2 location1 location2) [1]
03: (          pick arm1 leg4 location1) [1]
04: (move arm1 leg4 location1 location2) [1]
05: (drop arm1 leg4 location1 location2) [1]
06: (          pick arm1 leg1 location1) [1]
07: (move arm1 leg1 location1 location2) [1]
08: (drop arm1 leg1 location1 location2) [1]
09: (          pick arm1 leg3 location1) [1]
10: (move arm1 leg3 location1 location2) [1]
11: (drop arm1 leg3 location1 location2) [1]

plan total cost: 12.00

```

